from bisect import bisect_left


class Timeframe:

    def __init__(self, _time, _altitude, _x, _y, _velocity, _power):
        self.time = _time
        self.altitude = _altitude
        self.x = _x
        self.y = _y
        self.velocity = _velocity
        self.power = _power

    def get_value(self):
        return self.altitude, self.x, self.y, self.velocity, self.power


class Timeline:

    def __init__(self):
        self.timeline = []

    def get_value(self, timepoint):
        index = self.find_node(timepoint)
        return self.timeline[index].get_value()

    # unused
    def get_values(self, start, end):
        left = self.find_node(start)
        right = self.find_node(end)
        values = []
        for node in range(left, right):
            values.append(self.timeline[node])
        return values

    def get_altitude(self):
        values = []
        for node in self.timeline:
            values.append(node.altitude)
        return values

    def get_velocity(self):
        values = []
        for node in self.timeline:
            values.append(node.velocity * 10)       # converts from [km/h] into [hm/h] for better plot clarity
        return values

    def get_time(self):
        values = []
        time_zero = self.timeline[0].time
        for node in self.timeline:
            values.append(node.time - time_zero)    # subtract start time
        return values

    def get_x(self):
        values = []
        x_zero = self.timeline[0].x
        for node in self.timeline:
            values.append(node.x - x_zero)          # subtract initial position
        return values

    def get_y(self):
        values = []
        y_zero = self.timeline[0].y
        for node in self.timeline:
            values.append(node.y - y_zero)          # subtract initial position
        return values

    def find_node(self, timepoint):
        indexes = []
        for node in self.timeline:
            indexes.append(node.time)
        return bisect_left(indexes, timepoint)      # searches for the closest timeframe and returns its values

    def set_value(self, _time, _altitude, _x, _y, _velocity, _power):
        node = Timeframe(_time, _altitude, _x, _y, _velocity, _power)
        self.timeline.append(node)
