from tkinter import *

import os.path

import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

from Interface import *


class GUI:

    #
    #
    #                      GUI Layout
    # -------------------------------------------------------
    #  -----------------------------------------------------
    #  |                   MASTERFRAME                     |
    #  |  ---------------------- ------------------------  |
    #  |  |       FRAME1       | |        FRAME2        |  |
    #  |  |  subframe1_upper   | |                      |  |
    #  |  |  subframe1_lower   | |                      |  |
    #  |  ---------------------- ------------------------  |
    #  -----------------------------------------------------
    #  -----------------------------------------------------
    #  |                      FRAME3                       |
    #  |  -----------------------------------------------  |
    #  |  |              SUBFRAME2_UPPER                |  |
    #  |  |--------------------   ----------------------|  |
    #  |  |  SUBFRAME3_LEFT    | |    SUBFRAME3_RIGHT   |  |
    #  |  |   altitudePlot     | |      positionPlot    |  |
    #  |  -----------------------------------------------  |
    #  |  -----------------------------------------------  |
    #  |  |              SUBFRAME2_LOWER                |  |
    #  |  |              flightCoursePlot               |  |
    #  |  -----------------------------------------------  |
    #  -----------------------------------------------------
    #  -----------------------------------------------------
    #  |                     FRAME4                        |
    #  |                     button                        |
    #  |                     footer                        |
    #  -----------------------------------------------------
    #

    def __init__(self):

        # initializes main window, makes it not resizable
        self.top = Tk()
        self.top.resizable(width=FALSE, height=FALSE)
        self.interface = Interface()

        #
        ### FRAMES AND STRUCTURES ###
        #

        # initializing Tkinter.Frames to which Tkinter objects will be bound
        #  masterFrame (TOP): container for frames 1 and 2; visible at the top of the interface
        #  frame1 (TOP LEFT): container for parameter indicators
        #  frame2 (TOP RIGHT): container for a list of timeframes
        #  frame3 (MIDDLE): container for plots
        #  frame4 (BOTTOM): container for footers and Tkinter.Button RUN button (frame4)
        #
        self.frame4 = Frame(self.top)
        self.frame3 = Frame(self.top)
        self.masterFrame = Frame(self.top)
        self.frame1 = Frame(self.masterFrame, relief=RIDGE, borderwidth=1)
        self.frame2 = Frame(self.masterFrame, relief=RIDGE, borderwidth=1, height=50, width=50)

        self.frame4.pack(side=BOTTOM)
        self.frame3.pack(side=BOTTOM)
        self.masterFrame.pack()
        self.frame1.pack(side=LEFT, pady=15, padx=3)
        self.frame2.pack(side=RIGHT, pady=15, padx=3)

        #
        ### frame4: FOOTER ###
        #

        # initializing button which toggles the Interface.toggle_button() and Interface.read_from_file() on click
        #  currently displayed text is stored in self.buttonText, default is "Run", switched to "Cancel" after click
        #  button (frame4, TOP)
        #
        self.buttonText = StringVar()
        self.buttonText.set("Run")
        self.button = Button(self.frame4, textvariable=self.buttonText, command=lambda: Interface.toggle_button(self.interface, self))
        self.button.pack()

        # initializing footer text
        #  header1, header2 (frame4, BOTTOM)
        #
        self.header1 = Label(self.frame4, text="Flight parameters recorder")
        self.header2 = Label(self.frame4, text="© Wiktor Wudarczyk 2016")
        self.header1.pack(side=BOTTOM)
        self.header2.pack(side=BOTTOM)

        #
        ### frame1: INDICATORS ###
        #

        # initializing subframes to be placed on top of each other in Tkinter.Frame frame1 to form two rows
        #  subframe1_upper (frame1, TOP)
        #  subframe1_lower (frame1, BOTTOM)
        #
        self.subframe1_upper = Frame(self.frame1)
        self.subframe1_lower = Frame(self.frame1)
        self.subframe1_lower.pack(pady=10)
        self.subframe1_upper.pack(pady=10)

        # initializing Tkinter.StringVars for Tkinter.Entries (frame1) to read data from
        self.altitudeVar = StringVar()
        self.xVar = StringVar()
        self.yVar = StringVar()
        self.velocityVar = StringVar()
        self.powerVar = StringVar()

        # initializing Tkinter.Entries which show flight parameters
        #  altitudeEntry, velocityEntry, powerEntry (frame1, subframe1_upper, TOP)
        #  xEntry, yEntry (frame1, subframe1_lower, BOTTOM)
        #
        self.altitudeEntry = Entry(self.subframe1_upper, bd=5, width=6, textvariable=self.altitudeVar)
        self.velocityEntry = Entry(self.subframe1_upper, bd=5, width=4, textvariable=self.velocityVar)
        self.powerEntry = Entry(self.subframe1_upper, bd=5, width=4, textvariable=self.powerVar)
        self.xEntry = Entry(self.subframe1_lower, bd=5, width=12, textvariable=self.xVar)
        self.yEntry = Entry(self.subframe1_lower, bd=5, width=12, textvariable=self.yVar)

        # initializing Tkinter.Labels showing parameter names, placed next to its corresponding Tkinter.Entries
        #  altitudeLabel, velocityLabel, powerLabel (frame1, subframe1_upper, TOP)
        #  xLabel, yLabel (frame1, subframe1_lower, BOTTOM)
        #
        self.altitudeLabel = Label(self.subframe1_upper, text="altitude")
        self.velocityLabel = Label(self.subframe1_upper, text="velocity")
        self.powerLabel = Label(self.subframe1_upper, text="power")
        self.xLabel = Label(self.subframe1_lower, text="x")
        self.yLabel = Label(self.subframe1_lower, text="y")

        # inserting initial values into Tkinter.StringVars shown by Tkinter.Entries
        self.altitudeEntry.insert(END, "0.0")
        self.velocityEntry.insert(END, "0.0")
        self.powerEntry.insert(END, "0.0")
        self.xEntry.insert(END, "0.0")
        self.yEntry.insert(END, "0.0")

        # packing elements into frame1
        self.altitudeLabel.pack(side=LEFT)
        self.altitudeEntry.pack(side=LEFT)
        self.velocityLabel.pack(side=LEFT)
        self.velocityEntry.pack(side=LEFT)
        self.powerLabel.pack(side=LEFT)
        self.powerEntry.pack(side=LEFT)
        self.xLabel.pack(side=LEFT)
        self.xEntry.pack(side=LEFT)
        self.yLabel.pack(side=LEFT)
        self.yEntry.pack(side=LEFT)

        #
        ### frame2: TIMEFRAME LIST ###
        #

        # action activated by clicking on an entry in Tkinter.Listbox timeframeSelector
        # loads parameters from given timeframe into Tkinter.Entry (frame1) indicators
        def onclick_listbox(event):
            refresh(self, float(self.timeframeSelector.get(self.timeframeSelector.curselection())) + self.interface.Flight.timeline[0].time)

        # initializing Tkinter.Listbox and its Tkinter.Scrollbar and Tkinter.Label
        #  timeframeSelector, scroll, scrollLabel (frame2, RIGHT)
        #
        self.timeframeSelector = Listbox(self.frame2, height=4, width=10)
        self.scroll = Scrollbar(self.frame2, command =self.timeframeSelector.yview)
        self.timeframeSelector.config(yscrollcommand=self.scroll.set)
        self.timeframeSelector.bind("<ButtonRelease-1>", onclick_listbox)
        self.scrollLabel = Label(self.frame2, text="Time [s]:")

        self.scroll.pack(expand=YES, fill=Y, side=RIGHT)
        self.timeframeSelector.pack(side=RIGHT)
        self.scrollLabel.pack(side=TOP)

    #
    # function triggered by Interface.toggle_button() when processed log ends
    # initialises and draws plots using data extracted from Interface.Timeframe
    #
    def draw_plot(self):

        # decreases font size for all plots for enhanced clarity
        matplotlib.rcParams.update({'font.size': 9})

        ### frame3: FRAMES ###

        # initializes Tkinter.Frames to be placed in Tkinter.Frame frame3 for clearer layout
        self.subframe2_upper = Frame(self.frame3)
        self.subframe2_lower = Frame(self.frame3)
        self.subframe3_left = Frame(self.subframe2_upper)
        self.subframe3_right = Frame(self.subframe2_upper)

        self.subframe2_lower.pack(side=BOTTOM, pady=5)
        self.subframe2_upper.pack(side=BOTTOM)
        self.subframe3_left.pack(side=LEFT, padx=15)
        self.subframe3_right.pack(side=RIGHT, padx=15)

        #
        ### frame3: PLOTS ##
        #

        ## 1. altitudePlot ##

        # initializes and plots matplotlib.subplot altitudePlot showing altitude and speed in time
        #  canvas1 matplotlib.FigureCanvasTkAgg (frame3, subframe3_left, LEFT)
        #  f1      matplotlib.Figure (frame3, subframe3_left, canvas1, LEFT)
        #  altitudePlot (frame3, subframe3_left, canvas1, f1, LEFT)
        f1 = Figure(figsize=(6, 4), dpi=80)
        altitudePlot = f1.add_subplot(111)
        altitudePlot.set_xlabel('Time [s]')
        altitudePlot.set_ylabel('Altitude [m.a.s.l.](blue)/ Speed [hm/h](red)')
        altitudePlot.plot(self.interface.Flight.get_time(), self.interface.Flight.get_altitude())
        altitudePlot.plot(self.interface.Flight.get_time(), self.interface.Flight.get_velocity(), 'r')
        canvas1 = FigureCanvasTkAgg(f1, master=self.subframe3_left)
        canvas1.show()
        canvas1.get_tk_widget().pack(side=LEFT)
        f1.savefig(self.interface.path + "__1.png")

        # makes altitudePlot interactive, activated on click
        # loads parameters from given timeframe into Tkinter.Entry (frame1) indicators
        def onclick_plot(event):
            refresh(self, event.xdata + self.interface.Flight.timeline[0].time)
        f1.canvas.mpl_connect('button_press_event', onclick_plot)

        #
        ## 2. positionPlot ##

        # initializes and plots matplotlib.subplot positionPlot showing x and y position regardless of timeframe
        #  canvas2 matplotlib.FigureCanvasTkAgg (frame3, subframe3_left, RIGHT)
        #  f2      matplotlib.Figure (frame3, subframe3_left, canvas2, RIGHT)
        #  positionPlot (frame3, subframe3_left, canvas2, f2, RIGHT)
        f2 = Figure(figsize=(6, 4), dpi=80)
        positionPlot = f2.add_subplot(111)
        positionPlot.set_xlabel('X [m]')
        positionPlot.set_ylabel('Y [m]')
        positionPlot.plot(self.interface.Flight.get_x(), self.interface.Flight.get_y())
        canvas2 = FigureCanvasTkAgg(f2, master=self.subframe3_right)
        canvas2.show()
        canvas2.get_tk_widget().pack(side=RIGHT)
        f2.savefig(self.interface.path + "__2.png")

        #
        ## 3. flightCoursePlot ##

        # initializes 3D and plots matplotlib.subplot flightCoursePlot showing flight course (x, y, altitude)
        #  canvas2 matplotlib.FigureCanvasTkAgg (frame3, subframe3_left, RIGHT)
        #  f2      matplotlib.Figure (frame3, subframe3_left, canvas2, RIGHT)
        #  positionPlot (frame3, subframe3_left, canvas2, f2, RIGHT)
        #
        f3 = Figure(figsize=(5, 4), dpi=110)
        flightCoursePlot = f3.gca(projection='3d')
        z = self.interface.Flight.get_altitude()
        x = self.interface.Flight.get_x()
        y = self.interface.Flight.get_y()
        flightCoursePlot.plot(x, y, z, label='Flight course')
        flightCoursePlot.legend()
        canvas3 = FigureCanvasTkAgg(f3, master=self.subframe2_lower)
        canvas3.show()
        canvas3.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
        Axes3D.mouse_init(flightCoursePlot)

        # saves flightCoursePlot rotated (or not) freely by user
        # path is the name of log + "__3_%d.png", with "%d" being the first free natural number
        #
        def print_plot():
            path = self.interface.path + "__3_1.png"
            i = 1
            while os.path.isfile(path):
                i += 1
                path = self.interface.path + "__3_%d.png" % i
            f3.savefig(path)
            self.plotToFileText.set(path + " successfully saved!")

        # initialization of Tkinter.Button used to activate print_plot() function and Tkinter.Label showing the path of saved file
        #  plotToFileButton (frame3, subframe2_lower, BOTTOM)
        #
        plotToFileButton = Button(self.subframe2_lower, text="Print", command= print_plot)
        plotToFileButton.pack(side=BOTTOM)
        self.plotToFileText = StringVar()
        plotToFileLabel = Label(self.subframe2_lower, textvariable=self.plotToFileText)
        plotToFileLabel.pack()

        self.top.update()

    # function used to pass given data into Tkinter.Entries from Tkinter.Label label1
    def draw_data(self, altitude, x, y, velocity, power):
        self.altitudeVar.set(str(altitude))
        self.xVar.set(str(x))
        self.yVar.set(str(y))
        self.velocityVar.set(str(velocity))
        self.powerVar.set(str(power))
        self.top.update()

# function used to pass data loaded from Interface.Timeline for given timeframeinto Tkinter.Entries from Tkinter.Label label1
def refresh(GUI_instance, timeframe):

        data = GUI_instance.interface.Flight.get_value(timeframe)
        GUI_instance.draw_data(*data)