import sys



class ApplicationMgr:

    def equation(degree):

        equationArray = []

        for iterator in range(degree):
            isEquasionOK = False
            eqTemp = None
            while not (isEquasionOK):
                eqTemp = InputReader.equationMaker(iterator, degree)
                isEquasionOK = InputValidator.validate(*eqTemp[0])
            equationArray += eqTemp

        Solver.solveEquation2ndDegree(equationArray)



class InputReader:

    def equationMaker(iterator, degree):

        equation = []
        for iterator2 in range(degree):
            x = float(input("Podaj wspolczynnik przy zmiennej %s w rownaniu %s: " % (str(iterator2+1), str(iterator+1))))
            equation.append(x)
        rest = float(input("Podaj wyraz wolny w rownaniu %s: " % str(iterator + 1)))
        equation.append(rest)
        return [equation]



class InputValidator:

    def validate(*equation):
        for x in equation:
            if not (isinstance(x, float)):
                print(x + " nie jest liczba!")
                return False
        return True



class Solver:

    def det1(x1):
        return x1
    def det2(x1, y1, x2, y2):
        return x1 * y2 - x2 * y1

    def solveEquation2ndDegree(arr):
        matrix0 = (arr[0][0], arr[0][1], arr[1][0], arr[1][1])
        if not (Solver.det2(*matrix0)):
            print("Rozwiazaniem jest zbior liczb rzeczywistych")
            sys.exit()
        matrix1 = (arr[0][2], arr[0][1], arr[1][2], arr[1][1])
        matrix2 = (arr[0][0], arr[0][2], arr[1][0], arr[1][2])
        x = Solver.det2(*matrix1) / Solver.det2(*matrix0)
        y = Solver.det2(*matrix2) / Solver.det2(*matrix0)
        print("x: " + str(x) + " y: " + str(y))



instance = ApplicationMgr.equation(2)
